Pyramid Push is a 3D platformer developed using C++ and DirectX 11, using Microsoft Visual Studio 2013, as part of
my "Program 3D Applications" unit in my second year of university.

This was the first project I had undertaken that utilised both DirectX 11 and source control with Git.

During the process of development I learned to constantly commit changes to source control, and resolve merge
conflicts.