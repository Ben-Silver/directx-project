#include "textureBuffer.h"

textureBuffer::textureBuffer()
{
}

textureBuffer::~textureBuffer()
{
}

void textureBuffer::texName(char *name)
{
	// Normally this will apply a single texture to all objects
	// using this will set the device, buffer and texture name so
	// so different textures can be applied to several objects
	D3DX11CreateShaderResourceViewFromFile(obj, name, NULL, NULL, &tex, NULL);
}

void textureBuffer::setDevice(ID3D11Device *obj, ID3D11ShaderResourceView *tex)
{
	this->obj = obj;
	this->tex = tex;
}
