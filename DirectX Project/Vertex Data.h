//#include "DirectXPointers.h"
#include "GameObject.h"
#include "Vertex.h"

// I had hoped that I could move all the vertex data into a class and pass it into main.cpp,
// unfortunately I was unable to do so successfully
class vertexData
{
public:
	ID3D11Device* dev;
	ID3D11DeviceContext *devcon;

	float positionX;
	float positionY;
	float positionZ;

	//std::vector<GameObject> list;

	textureBuffer texture;

	// Parallax vector
	std::vector<Vertex> parallaxVerts;

	// Scorebox Vector
	std::vector<Vertex> scoreVerts;

	// scoreNumber Vector
	std::vector<Vertex> scoreNumVerts;

	// Player (cube) vector
	std::vector<Vertex> cubeVerts;

	// pyramid vector
	std::vector<Vertex> pyramidVerts;

	// platform vector
	std::vector<Vertex> platVerts;

	// bullet vector
	std::vector<Vertex> bulletVerts;

	vertexData();
};