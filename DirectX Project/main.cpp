// Basic Windows Headers
#include <windows.h>
#include <windowsx.h>
#include <vector>
#include <math.h>

// Direct3D Libraries
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")

// Our Headers
#include "DirectXPointers.h"		// Contains several D3DX Pointers - it makes for less clutter in the main.cpp file
#include "Vertex.h"
#include "GameObject.h"
#include "textureBuffer.h"
#include "scoreBox.h"
#include "Vertex Data.h"

#ifdef FULL_SCREEN
	// define the screen resolution
	#define SCREEN_WIDTH  1920
	#define SCREEN_HEIGHT 1080
	#define WINDOWED FALSE
#else
	#define SCREEN_WIDTH  800
	#define SCREEN_HEIGHT 600
	#define WINDOWED TRUE
#endif

#define SPEED_MAX 0.004f				 // Maximum vertical speed the player can reach
#define NUM_CUBES_CUBE_ROOTED 1			 // 37^3 ~= 50,000
#define NUMBER_OF_BULLETS 13			 // Total number of bullets - 11
#define bulletCounterMax 30.0f			 // If the bulletCounter exceeds this number, the bullet's position will reset

// a struct to define a single vertex
struct VERTEX
{
	FLOAT X, Y, Z; D3DXVECTOR3 Normal; FLOAT U, V;
};

// a struct to define the constant buffer
struct CBUFFER
{
    D3DXMATRIX Final;
    D3DXMATRIX Rotation;
    D3DXVECTOR4 LightVector;
    D3DXCOLOR LightColor;
    D3DXCOLOR AmbientColor;
};

// Global Variables
std::vector<gameObject> list;			 // Initialising the Game Object - all the objects come from this
scoreBox scoreBoard;					 // Initialising the score board
float bulletCounter = 0.0f;				 // This will count up and reset when it exceets the value of bulletCounterMax
int updateCube = 0;						 // Tracks the number of times the coin box has been hit
bool bulletMoving = false;				 // If the bullet is not in it's original coodinates, this will be set to true

float cameraX = 0.0f;					 // X value of the camera - made for debugging purposes
float cameraY = 0.0f;					 // Y value of the camera - made for debugging purposes
float cameraZ = 0.0f;					 // Z value of the camera - made for debugging purposes

// function prototypes
void InitD3D(HWND hWnd);				 // sets up and initializes Direct3D
void CleanD3D(void);					 // closes Direct3D and releases memory
void InitPipeline();

// Function Prototypes
void InitD3D(HWND hWnd);
void CleanD3D();
void RenderFrame(void);
void update(void);

void VSSetConstantBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *ppConstantBuffers);

void UpdateSubresource(
	ID3D11Resource *pDstResource,		 // address of the buffer to update
	UINT DstSubresource,				 // advanced
	D3D11_BOX *pDstBox,					 // advanced
	void *pSrcData,						 // address of the struct containing the variables
	UINT SrcRowPitch,					 // advanced
	UINT SrcDepthPitch);				 // advanced

// the WindowProc function prototype
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam,	LPARAM lParam);

// the entry point for any Windows program
// Key Bindings are done here
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// the handle for the window, filled by a function
	HWND hWnd;
	// this struct holds information for the window class
	WNDCLASSEX wc;

	// clear out the window class for use
	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	// fill in the struct with the needed information
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = "WindowClass1";

	// register the window class
	RegisterClassEx(&wc);

	// calculate the size of the client area
	RECT wr = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };		// set the size, but not the position
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);		// adjust the size

	// create the window and use the result as the handle
	hWnd = CreateWindowEx(NULL,
		"WindowClass1",										// name of the window class
		"Pyramid Push",										// title of the window
		WS_OVERLAPPEDWINDOW,								// window style
		32,													// x-position of the window
		32,													// y-position of the window
		wr.right - wr.left,									// width of the window
		wr.bottom - wr.top,									// height of the window
		NULL,												// we have no parent window, NULL
		NULL,												// we aren't using menus, NULL
		hInstance,											// application handle
		NULL);												// used with multiple windows, NULL

	// display the window on the screen
	ShowWindow(hWnd, nCmdShow);

// enter the main loop:

	// this struct holds Windows event messages
	MSG msg = { 0 };

	// function calls
	InitD3D(hWnd);
	InitPipeline();

	// Enter the infinite message loop
	while (TRUE)
	{
		// Check to see if any messages are waiting in the queue
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc function
			DispatchMessage(&msg);

			// check to see if it's time to quit
			if (msg.message == WM_QUIT)
				break;

			// Key down key bindings
			else if (msg.message == WM_KEYDOWN)
			{
				if (msg.wParam == 'D')
				{
					list[0].setSlowDown(false);
					list[0].setSpeedUp(true);
					list[0].setDPressed(true);
				}
				else if (msg.wParam == 'A')
				{
					list[0].setSlowDown(false);
					list[0].setSpeedUp(true);
					list[0].setAPressed(true);
				}
				else if (msg.wParam == 'Q')
				{
					list[0].setTurn(true);
					list[0].setTurnLeft(true);
				}
				else if (msg.wParam == 'E')
				{
					list[0].setTurn(true);
					list[0].setTurnRight(true);
				}
				else if ((msg.wParam == 'W') && list[0].canJump)
				{
					list[0].setJump(true);
					list[0].setJumping(true);
				}
			}

			// Key up key bindings
			else if (msg.message == WM_KEYUP)
			{
				if (msg.wParam == 'D')
				{
					list[0].setSpeedUp(false);;
					list[0].setSlowDown(true);
					list[0].setDPressed(false);
				}
				else if (msg.wParam == 'A')
				{
					list[0].setSpeedUp(false);
					list[0].setSlowDown(true);
					list[0].setAPressed(false);
				}
				else if (msg.wParam == 'Q')
				{
					list[0].setTurn(false);
					list[0].setTurnLeft(false);
				}
				else if (msg.wParam == 'E')
				{
					list[0].setTurn(false);
					list[0].setTurnRight(false);
				}
			}
		}

		// Calls the update and render functions every frame
		RenderFrame();
		update();
	}

	// return this part of the WM_QUIT message to Windows
	return msg.wParam;
}

// this is the main message handler for the program
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// sort through and find what code to run for the message given
	switch (message)
	{
		// this message is read when the window is closed
		case WM_DESTROY: // DESTROOOYYYY!!!!!
		{
			// close the application entirely
			PostQuitMessage(0);
			return 0;
		}
		break;
	}

	// Handle any messages the switch statement didn't
	return DefWindowProc(hWnd, message, wParam, lParam);
}

// this function initializes and prepares Direct3D for use
// Contains vertex/index data and index buffer
void InitD3D(HWND hWnd)
{
	// create a struct to hold information about the swap chain
	DXGI_SWAP_CHAIN_DESC scd;

	// clear out the struct for use
	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

	// fill the swap chain description struct
	scd.BufferCount = 1;                                    // one back buffer
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;     // use 32-bit color
	scd.BufferDesc.Width = SCREEN_WIDTH;
	scd.BufferDesc.Height = SCREEN_HEIGHT;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;      // how swap chain is to be used
	scd.OutputWindow = hWnd;                                // the window to be used
	scd.SampleDesc.Count = 4;                               // how many multisamples
	scd.Windowed = WINDOWED;                                // windowed/full-screen mode
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;     // allow full-screen switching

	// create a device, device context and swap chain using the information in the scd struct
	D3D11CreateDeviceAndSwapChain(NULL,	D3D_DRIVER_TYPE_HARDWARE, NULL,	NULL, NULL,	NULL, D3D11_SDK_VERSION, &scd, &swapchain, &dev, NULL, &devcon);

	// create the depth buffer texture
	D3D11_TEXTURE2D_DESC texd;
	ZeroMemory(&texd, sizeof(texd));

	texd.Width = SCREEN_WIDTH;
	texd.Height = SCREEN_HEIGHT;
	texd.ArraySize = 1;
	texd.MipLevels = 1;
	texd.SampleDesc.Count = 4;
	texd.Format = DXGI_FORMAT_D32_FLOAT;
	texd.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	ID3D11Texture2D *pDepthBuffer;
	dev->CreateTexture2D(&texd, NULL, &pDepthBuffer);

	// create the depth buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	ZeroMemory(&dsvd, sizeof(dsvd));

	dsvd.Format = DXGI_FORMAT_D32_FLOAT;
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	dev->CreateDepthStencilView(pDepthBuffer, &dsvd, &zbuffer);
	pDepthBuffer->Release();

	// get the address of the back buffer
	ID3D11Texture2D *pBackBuffer;
	swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

	// use the back buffer address to create the render target
	dev->CreateRenderTargetView(pBackBuffer, NULL, &backbuffer);
	pBackBuffer->Release();

	// set the render target as the back buffer
	devcon->OMSetRenderTargets(1, &backbuffer, zbuffer); // NULL);

	// Set the viewport
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = SCREEN_WIDTH;
	viewport.Height = SCREEN_HEIGHT;
	viewport.MinDepth = 0;    // the closest an object can be on the depth buffer is 0.0
	viewport.MaxDepth = 1;    // the farthest an object can be on the depth buffer is 1.0

	devcon->RSSetViewports(1, &viewport);

	//==================================================================================================================================
	// Vertex Data
	//----------------------------------------------------------------------------------------------------------------------------------
	// First, a vector array for the vertices is declared, then vertex data is pushed to said array
	// The vertices are declared like this: std::vector<Vertex> myObjectVertices
	//
	// Where myObjectVertices is the name you give the array
	//----------------------------------------------------------------------------------------------------------------------------------
	// The data pushed to the array is pushed with this format:
	// myObjectVertices.push_back(Vertex(x, y, z, D3DXVECTOR3(Red, Green, Blue), u, v))
	//
	// Where x, y, z, Red, Green, Blue, u, v are all floats
	//----------------------------------------------------------------------------------------------------------------------------------
	// After the vetices have been defined, a GameObject object is defined by passing data to it
	// in this format: GameObject myObjectName(device, deviceContext, myObjectVertices);
	//
	// Where device is an ID3D11Device, deviceContext is an ID3D11DeviceContext and myObjectVertices,
	// is defined prior.
	//----------------------------------------------------------------------------------------------------------------------------------
	// After creating the game object, the x, y, and z coordinates must be assigned to it, along with
	// the texture. It must finally be pushed to the array, list, so it can be passed through the
	// Vertex Buffer and rendered.
	// The following format is how I have layed out my objects:
	// myObjectName.positionX = x
	// myObjectName.positionY = y
	// myObjectName.positionZ = z
	// myObjectName.texture.setDevice(dev, pTexture);
	// myObjectName.texture.texName("textureName.png");
	// list.push_back(myObjectName);
	//
	// Where x, y and z are floats to be assigned, dev is an ID3D11Device, pTexture is an ID3D11ShaderResourceView,
	// textureName is the name of a bitmap and myObjectName is the name of the object to be pushed to list.
	//==================================================================================================================================

	// Parallax vector
	std::vector<Vertex> parallaxVerts;
	parallaxVerts.push_back(Vertex(-100.0f, -100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));
	parallaxVerts.push_back(Vertex(-100.0f, 100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	parallaxVerts.push_back(Vertex(100.0f, -100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	parallaxVerts.push_back(Vertex(100.0f, 100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	// Scorebox Vector
	std::vector<Vertex> scoreVerts;
	scoreVerts.push_back(Vertex(-0.6f, -0.6f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));
	scoreVerts.push_back(Vertex(-0.6f, 0.6f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	scoreVerts.push_back(Vertex(2.0f, -0.6f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	scoreVerts.push_back(Vertex(2.0f, 0.6f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	// scoreNumber Vector
	std::vector<Vertex> scoreNumVerts;
	scoreNumVerts.push_back(Vertex(-0.2f, -0.2f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));
	scoreNumVerts.push_back(Vertex(-0.2f, 0.2f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	scoreNumVerts.push_back(Vertex(0.2f, -0.2f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	scoreNumVerts.push_back(Vertex(0.2f, 0.2f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	// Player (cube) vector
	std::vector<Vertex> cubeVerts;
	cubeVerts.push_back(Vertex(-1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 0.0f));    // side 1
	cubeVerts.push_back(Vertex(1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));    // side 2
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 0.0f));   // side 3
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f));    // side 4
	cubeVerts.push_back(Vertex(1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(-1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(1.0f, -1.0f, -1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 5
	cubeVerts.push_back(Vertex(1.0f, 1.0f, -1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(1.0f, -1.0f, 1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, 1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, -1.0f, -1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 6
	cubeVerts.push_back(Vertex(-1.0f, -1.0f, 1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, -1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, 1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	// pyramid vector
	std::vector<Vertex> pyramidVerts;
	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f));    // base
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));    // side 1
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, -0.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, -0.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(1.5f, -1.0f, -1.5f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 2
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, -0.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, 1.5f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, 0.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 0.0f));    // side 3
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, 0.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, 0.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, -1.5f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 4
	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, 1.5f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, -0.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, 0.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	// platform vector
	std::vector<Vertex> platVerts;
	platVerts.push_back(Vertex(-5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 0.0f));    // side 1
	platVerts.push_back(Vertex(5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(-5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));    // side 2
	platVerts.push_back(Vertex(-5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 0.0f));   // side 3 - Top Base
	platVerts.push_back(Vertex(-5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f));    // side 4
	platVerts.push_back(Vertex(5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(-5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(5.0f, -1.0f, -5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 5
	platVerts.push_back(Vertex(5.0f, 1.0f, -5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(5.0f, -1.0f, 5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, 5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, -1.0f, -5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 6
	platVerts.push_back(Vertex(-5.0f, -1.0f, 5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(-5.0f, 1.0f, -5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(-5.0f, 1.0f, 5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	// bullet vector
	std::vector<Vertex> bulletVerts;
	bulletVerts.push_back(Vertex(-0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 0.0f, 0.0f));    // side 1
	bulletVerts.push_back(Vertex(0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 0.0f, 0.0f));    // side 2
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 0.0f, 0.0f));   // side 3
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 0.0f, 0.0f));    // side 4
	bulletVerts.push_back(Vertex(0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(-0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 5
	bulletVerts.push_back(Vertex(0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, -0.3f, -0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 6
	bulletVerts.push_back(Vertex(-0.3f, -0.3f, 0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, -0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, 0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 1.0f, 1.0f));
	
	// Initialise Game Objects using vertices
	gameObject player(dev, devcon, pyramidVerts); // cubeVerts);
	gameObject platform(dev, devcon, platVerts);
	gameObject platform2(dev, devcon, platVerts);
	gameObject platform3(dev, devcon, platVerts);
	gameObject platform4(dev, devcon, platVerts);
	gameObject platform5(dev, devcon, platVerts);
	gameObject platform6(dev, devcon, platVerts);
	gameObject platform7(dev, devcon, platVerts);
	gameObject goal(dev, devcon, cubeVerts);
	gameObject parallax(dev, devcon, parallaxVerts);
	gameObject scoreBox(dev, devcon, scoreVerts);
	gameObject turret1(dev, devcon, cubeVerts);
	gameObject bullet1(dev, devcon, bulletVerts);
	gameObject bullet2(dev, devcon, bulletVerts);
	gameObject bullet3(dev, devcon, bulletVerts);
	gameObject scoreNum0(dev, devcon, scoreNumVerts);
	gameObject scoreNum1(dev, devcon, scoreNumVerts);
	gameObject scoreNum2(dev, devcon, scoreNumVerts);
	gameObject scoreNum3(dev, devcon, scoreNumVerts);
	gameObject scoreNum4(dev, devcon, scoreNumVerts);
	gameObject scoreNum5(dev, devcon, scoreNumVerts);
	gameObject scoreNum6(dev, devcon, scoreNumVerts);
	gameObject scoreNum7(dev, devcon, scoreNumVerts);
	gameObject scoreNum8(dev, devcon, scoreNumVerts);
	gameObject scoreNum9(dev, devcon, scoreNumVerts);

	// Push Game Objects to the Vertex Buffer
	for (int x = 0; x < NUM_CUBES_CUBE_ROOTED; x++)
	{
		for (int y = 0; y < NUM_CUBES_CUBE_ROOTED; y++)
		{
			for (int z = 0; z < NUM_CUBES_CUBE_ROOTED; z++)
			{
				// Player
				player.positionX = x;
				player.positionY = y - 4;
				player.positionZ = z;
				player.texture.setDevice(dev, pTexture);
				player.texture.texName("playerTex.png");
				list.push_back(player);

				// Platforms
				platform.positionX = x;
				platform.positionY = y - 6;
				platform.positionZ = z;
				platform.texture.setDevice(dev, pTexture);
				platform.texture.texName("Neon.png");
				list.push_back(platform);

				platform2.positionX = x + 15;
				platform2.positionY = y - 3;
				platform2.positionZ = z;
				platform2.texture.setDevice(dev, pTexture);
				platform2.texture.texName("Neon.png");
				list.push_back(platform2);

				platform3.positionX = x + 30;
				platform3.positionY = y - 6;
				platform3.positionZ = z;
				platform3.texture.setDevice(dev, pTexture);
				platform3.texture.texName("Neon.png");
				list.push_back(platform3);

				platform4.positionX = x + 45;
				platform4.positionY = y - 12;
				platform4.positionZ = z;
				platform4.texture.setDevice(dev, pTexture);
				platform4.texture.texName("Neon.png");
				list.push_back(platform4);

				platform5.positionX = x + 60;
				platform5.positionY = y - 9;
				platform5.positionZ = z;
				platform5.texture.setDevice(dev, pTexture);
				platform5.texture.texName("Neon.png");
				list.push_back(platform5);

				platform6.positionX = x + 75;
				platform6.positionY = y - 18;
				platform6.positionZ = z;
				platform6.texture.setDevice(dev, pTexture);
				platform6.texture.texName("Neon.png");
				list.push_back(platform6);

				platform7.positionX = x + 60;
				platform7.positionY = y - 27;
				platform7.positionZ = z;
				platform7.texture.setDevice(dev, pTexture);
				platform7.texture.texName("Neon.png");
				list.push_back(platform7);

				// Goal
				goal.positionX = x + 45;
				goal.positionY = y - 6;
				goal.positionZ = z;
				goal.texture.setDevice(dev, pTexture);
				goal.texture.texName("Coin Box.png");
				list.push_back(goal);

				// Parallax
				parallax.positionX = x + 40;
				parallax.positionY = y - 50;
				parallax.positionZ = z + 50;
				parallax.texture.setDevice(dev, pTexture);
				parallax.texture.texName("Background.png");
				list.push_back(parallax);

				// Score Box
				scoreBox.positionX = x - 10;
				scoreBox.positionY = y;
				scoreBox.positionZ = z - 10;
				scoreBox.texture.setDevice(dev, pTexture);
				scoreBox.texture.texName("scoreBox.png");
				list.push_back(scoreBox);

				// Turrets
				turret1.positionX = x + 60;
				turret1.positionY = y - 7;
				turret1.positionZ = z;
				turret1.texture.setDevice(dev, pTexture);
				turret1.texture.texName("Turret.png");
				list.push_back(turret1);

				// Bullets
				bullet1.positionX = x + 60;
				bullet1.positionY = y - 7;
				bullet1.positionZ = z;
				bullet1.texture.setDevice(dev, pTexture);
				bullet1.texture.texName("Flare.png");
				list.push_back(bullet1);

				bullet2.positionX = x + 60;
				bullet2.positionY = y - 7;
				bullet2.positionZ = z;
				bullet2.texture.setDevice(dev, pTexture);
				bullet2.texture.texName("Flare.png");
				list.push_back(bullet2);

				bullet3.positionX = x + 60;
				bullet3.positionY = y - 7;
				bullet3.positionZ = z;
				bullet3.texture.setDevice(dev, pTexture);
				bullet3.texture.texName("Flare.png");
				list.push_back(bullet3);

				// Score Number 0
				scoreNum0.positionX = x - 10;
				scoreNum0.positionY = y;
				scoreNum0.positionZ = z - 11;
				scoreNum0.texture.setDevice(dev, pTexture);
				scoreNum0.texture.texName("0.png");
				list.push_back(scoreNum0);
				
				// Score Number 1
				scoreNum1.positionX = x - 10;
				scoreNum1.positionY = y;
				scoreNum1.positionZ = z - 11;
				scoreNum1.texture.setDevice(dev, pTexture);
				scoreNum1.texture.texName("1.png");
				list.push_back(scoreNum1);

				// Score Number 2
				scoreNum2.positionX = x - 10;
				scoreNum2.positionY = y;
				scoreNum2.positionZ = z - 11;
				scoreNum2.texture.setDevice(dev, pTexture);
				scoreNum2.texture.texName("2.png");
				list.push_back(scoreNum2);

				// Score Number 3
				scoreNum3.positionX = x - 10;
				scoreNum3.positionY = y;
				scoreNum3.positionZ = z - 11;
				scoreNum3.texture.setDevice(dev, pTexture);
				scoreNum3.texture.texName("3.png");
				list.push_back(scoreNum3);

				// Score Number 4
				scoreNum4.positionX = x - 10;
				scoreNum4.positionY = y;
				scoreNum4.positionZ = z - 11;
				scoreNum4.texture.setDevice(dev, pTexture);
				scoreNum4.texture.texName("4.png");
				list.push_back(scoreNum4);

				// Score Number 5
				scoreNum5.positionX = x - 10;
				scoreNum5.positionY = y;
				scoreNum5.positionZ = z - 11;
				scoreNum5.texture.setDevice(dev, pTexture);
				scoreNum5.texture.texName("5.png");
				list.push_back(scoreNum5);

				// Score Number 6
				scoreNum6.positionX = x - 10;
				scoreNum6.positionY = y;
				scoreNum6.positionZ = z - 11;
				scoreNum6.texture.setDevice(dev, pTexture);
				scoreNum6.texture.texName("6.png");
				list.push_back(scoreNum6);

				// Score Number 7
				scoreNum7.positionX = x - 10;
				scoreNum7.positionY = y;
				scoreNum7.positionZ = z - 11;
				scoreNum7.texture.setDevice(dev, pTexture);
				scoreNum7.texture.texName("7.png");
				list.push_back(scoreNum7);

				// Score Number 8
				scoreNum8.positionX = x - 10;
				scoreNum8.positionY = y;
				scoreNum8.positionZ = z - 11;
				scoreNum8.texture.setDevice(dev, pTexture);
				scoreNum8.texture.texName("8.png");
				list.push_back(scoreNum8);

				// Score Number 9
				scoreNum9.positionX = x - 10;
				scoreNum9.positionY = y;
				scoreNum9.positionZ = z - 11;
				scoreNum9.texture.setDevice(dev, pTexture);
				scoreNum9.texture.texName("9.png");
				list.push_back(scoreNum9);
			}
		}
	}

	// create the index buffer out of DWORDs
	DWORD OurIndices[] =
	{
		// Cube
		0, 1, 2,    // side 1
		2, 1, 3,
		4, 5, 6,    // side 2
		6, 5, 7,
		8, 9, 10,    // side 3
		10, 9, 11,
		12, 13, 14,    // side 4
		14, 13, 15,
		16, 17, 18,    // side 5
		18, 17, 19,
		20, 21, 22,    // side 6
		22, 21, 23,

		// Pyramid
		0, 1, 2,
		2, 0, 3,
		3, 2, 0,
		2, 4, 3,
		3, 4, 0,
		0, 4, 1,
		1, 4, 2,

	};

	// create the index buffer
	D3D11_BUFFER_DESC bd;    // redefinition
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(DWORD) * 36;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.MiscFlags = 0;

	//HRESULT r;
	dev->CreateBuffer(&bd, NULL, &pIBuffer);

	D3D11_MAPPED_SUBRESOURCE ms;    // redefinition
	devcon->Map(pIBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);    // map the buffer
	memcpy(ms.pData, OurIndices, sizeof(OurIndices));                   // copy the data
	devcon->Unmap(pIBuffer, NULL);

}

// this is the function that cleans up Direct3D and COM
void CleanD3D()
{
	swapchain->SetFullscreenState(FALSE, NULL);    // switch to windowed mode

	// close and release all existing COM objects
	
	pLayout->Release();
	pVS->Release();
	pPS->Release();
	zbuffer->Release();
	pCBuffer->Release();
	pIBuffer->Release();
	swapchain->Release();
	backbuffer->Release(); 
	dev->Release();
	devcon->Release();
}

// Axis-Aligned Bounding Box (or AABB) Collision detection
bool checkCollide(int x1, int y1, int o1Width, int o1Height, int x2, int y2, int o2Width, int o2Height)
{
	// Retrieved from http://stackoverflow.com/questions/6083626/box-collision-code

	// AABB 1
	int x1Min = x1;
	int x1Max = x1 + o1Width;
	int y1Max = y1 + o1Height;
	int y1Min = y1;

	// AABB 2
	int x2Min = x2;
	int x2Max = x2 + o2Width;
	int y2Max = y2 + o2Height;
	int y2Min = y2;

	// Collision tests
	if (x1Max < x2Min || x1Min > x2Max) return false;
	if (y1Max < y2Min || y1Min > y2Max) return false;

	return true;
}

// Resetting all relevant objects
void gameOver()
{
	list[0].setPlayerX(0.0f);
	list[0].setPlayerY(0.0f);
	list[0].setPlayerZ(0.0f);
	scoreBoard.score = 0;
	list[8].positionX = 45;
	list[8].positionY = -6;
	updateCube = 0;
	bulletMoving = false;
}

// Updating the Score Number on-screen
void checkScore(int score)
{
	if (score == 0)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 1)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 2)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 3)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 4)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 5)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 6)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 7)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 8)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() + 0.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() - 5.36f);
	}
	else if (score == 9)
	{
		// 0
		list[15].setPlayerZ(-11.0f);
		list[15].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[15].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 1
		list[16].setPlayerZ(-11.0f);
		list[16].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[16].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 2
		list[17].setPlayerZ(-11.0f);
		list[17].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[17].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 3
		list[18].setPlayerZ(-11.0f);
		list[18].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[18].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 4
		list[19].setPlayerZ(-11.0f);
		list[19].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[19].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 5
		list[20].setPlayerZ(-11.0f);
		list[20].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[20].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 6
		list[21].setPlayerZ(-11.0f);
		list[21].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[21].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 7
		list[22].setPlayerZ(-11.0f);
		list[22].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[22].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 8
		list[23].setPlayerZ(-11.0f);
		list[23].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[23].setPlayerY(list[10].getPlayerY() - 5.36f);

		// 9
		list[24].setPlayerZ(-11.0f);
		list[24].setPlayerX(list[10].getPlayerX() + 1.6f);
		list[24].setPlayerY(list[10].getPlayerY() + 0.36f);
	}
}

// updates the player's movement and checks collision - Called every frame
void update(void)
{
	// Key Bindings
	for (int i = 0; i < list.size(); i++)
	{
		// Movement
		if (list[0].getSpeedUp() == true)
		{
			// Moving Right
			if (list[0].getDPressed() == true)
			{
				list[0].setSpeed(0.002f); //(0.003f);
			}
			
			// Moving Left
			if (list[0].getAPressed() == true)
			{
				list[0].setSpeed(-0.002f); //(-0.003f);
			}
		}
		if (list[0].getSlowDown() == true)
		{
			// Stop Moving
			list[0].setSpeed(0.0f);
		}

		// Jumping
		if (list[0].getJump() == true)
		{
			// Increase vertSpeed until it exceeds SPEED_MAX
			list[0].vertSpeed += 0.000003f; //0.000005f;
			if (list[0].vertSpeed >= SPEED_MAX)
			{
				list[0].canJump = false;
				list[0].setJump(false);
				list[0].setJumping(false);
			}
		}
		else
		{
			// Falling to the ground
			list[0].vertSpeed -= 0.000003f; //0.000005f;
		}
		
		// AABB Collision Detection call
		bool collided1 = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[1].getPlayerX() - 2.0f, list[1].getPlayerY(), 6.0f, 2.1f);
		bool collided2 = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[2].getPlayerX() - 2.0f, list[2].getPlayerY(), 6.0f, 2.1f);
		bool collided3 = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[3].getPlayerX() - 2.0f, list[3].getPlayerY(), 6.0f, 2.1f);
		bool collided4 = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[4].getPlayerX() - 2.0f, list[4].getPlayerY(), 6.0f, 2.1f);
		bool collided5 = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[5].getPlayerX() - 2.0f, list[5].getPlayerY(), 6.0f, 2.1f);
		bool collided6 = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[6].getPlayerX() - 2.0f, list[6].getPlayerY(), 6.0f, 2.1f);
		bool collided7 = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[7].getPlayerX() - 2.0f, list[7].getPlayerY(), 6.0f, 2.1f);
		bool collidedTurret = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[11].getPlayerX(), list[11].getPlayerY(), 1.1f, 2.1f);
		bool goalReach = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[8].getPlayerX(), list[8].getPlayerY(), 2.0f, 2.0f);

		// Determines whether the player is on solid ground or not
		if ((collided1 == true) || (collided2 == true) || (collided3 == true) || (collided4 == true) || (collided5 == true) || (collided6 == true) || (collided7 == true) || (collidedTurret == true))
		{
			list[0].canJump = true;
			if (list[0].getJumping() == false)
			{
				list[0].vertSpeed = 0.0f;
			}
		}
		else
		{
			list[0].setJumping(false);
			list[0].setPlayerY(list[0].getPlayerY() + list[0].vertSpeed);
		}

		// Moving the bullets and detecting collision of the bullets
		for (int i = 12; i < NUMBER_OF_BULLETS; i++)
		{
			// Makes the bullet shoot towards the player
			if (list[0].getPlayerX() <= list[11].getPlayerX())
			{
				// If the bullet is already moving in a certain
				// direction, this makes it so that it will not
				// switch directions until a new one is spawned
				if (bulletMoving == false)
				{
					list[i].setSpeed(-0.0004);
				}
			}
			else if (list[0].getPlayerX() > list[11].getPlayerX())
			{
				// If the bullet is already moving in a certain
				// direction, this makes it so that it will not
				// switch directions until a new one is spawned
				if (bulletMoving == false)
				{
					list[i].setSpeed(0.0004);
				}
			}

			// Moves the bullet
			list[i].setPlayerX(list[i].getPlayerX() + list[i].getSpeed());

			// Resetting the bullet's position
			if (bulletCounter < bulletCounterMax)
			{
				// Increases the bulletCounter until it is greater than bulletCounterMax
				bulletCounter += 0.0004f;

				// As long as bulletCounter < bulletCounterMax, the bullet will continue moving
				bulletMoving = true;
			}
			else
			{
				// Resets the bullet's position
				list[i].positionX = 60.0f;

				// Resets the bulletCounter
				bulletCounter = 0.0f;

				// Makes it so the bullet is not moving
				bulletMoving = false;

			}

			// Checking whether the bullet has hit the player
			bool bulletHit = checkCollide(list[0].getPlayerX(), list[0].getPlayerY(), 2.0f, 2.0f, list[i].getPlayerX(), list[i].getPlayerY(), 0.4f, 0.4f);

			// If the bullet hits the player, the game resets
			if (bulletHit == true)
			{
				gameOver();
			}
			
			// End of bullet loop

		}

		// Checking to see if the player hits the coin box
		if (goalReach == true)
		{

			if (updateCube == 0)
			{
				list[8].positionX += 10;

				// increment the score
				scoreBoard.score += 1;
				updateCube += 1;
			}

			else if (updateCube == 1)
			{
				list[8].positionX += 10;
				scoreBoard.score += 1;
				updateCube += 1;
			}

			else if (updateCube == 2)
			{
				list[8].positionX += 10;
				list[8].positionY -= 10;
				scoreBoard.score += 1;
				updateCube += 1;
				//list.pop_back();
			}

			else if (updateCube == 3)
			{
				list[8].positionX -= 10;
				scoreBoard.score += 1;
				updateCube += 1;
				//list.pop_back();
			}
		}

		if (list[0].getPlayerY() < -100.0f)
		{
			gameOver();
		}

		checkScore(scoreBoard.score);

		// Moving the Player after movement values have been set
		list[0].setPlayerX(list[0].getPlayerX() + list[0].getSpeed());
		list[0].setPlayerY(list[0].getPlayerY() + list[0].vertSpeed);

		// Keeping the Score Box offset from the player
		list[10].setPlayerX(list[0].getPlayerX() - 10.0f);
		list[10].setPlayerY(list[0].getPlayerY() - 4.0f);
		
		// Uncomment for debugging purposes
		//cameraX -= 0.0001f;
		//cameraY += 0.0001f;
		//cameraZ += 0.0001f;
	}
}

// this is the function used to render a single frame
// contains camera data
void RenderFrame(void)
{
	CBUFFER cBuffer;
	cBuffer.LightVector = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 0.0f);
	cBuffer.LightColor = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	cBuffer.AmbientColor = D3DXCOLOR(0.4f, 0.4f, 0.4f, 1.0f);

	D3DXMATRIX matTranslate, matRotate, matView, matProjection;
	D3DXMATRIX matFinal;

	static float Time = 0.155f;
	//Time += 0.001f;

	// create a rotation matrix
	D3DXMatrixRotationY(&matRotate, Time);

	// create a view matrix
	D3DXMatrixLookAtLH(&matView,

		// the camera zoom position
		&D3DXVECTOR3(list[0].getPlayerX() - 5.0f + cameraX, list[0].getPlayerY() + 4.0f + cameraY, list[0].getPlayerZ() - 30.0f + cameraZ),
		
		// the look-at position
		&D3DXVECTOR3(list[0].getPlayerX(), list[0].getPlayerY(), 0.0f),

		// the up direction
		&D3DXVECTOR3(0.0f, 51.0f, 0.0f));

	// create a projection matrix
	D3DXMatrixPerspectiveFovLH(&matProjection,
		(FLOAT)D3DXToRadian(45),                    // field of view
		(FLOAT)SCREEN_WIDTH / (FLOAT)SCREEN_HEIGHT, // aspect ratio
		1.0f,                                       // near view-plane
		1000.0f);                                    // far view-plane

	// clear the back buffer to a deep blue
	devcon->ClearRenderTargetView(backbuffer, D3DXCOLOR(0.0f, 0.2f, 0.5f, 1.0f)); //(0.0f, 0.2f, 0.4f, 1.0f));

	// clear the depth buffer
	devcon->ClearDepthStencilView(zbuffer, D3D11_CLEAR_DEPTH, 1.0f, 0);

	// do 3D rendering on the back buffer here
	// rendering loop
	for (int i = 0; i < list.size(); i++)
	{
		// Translate every object in the list
		D3DXMatrixTranslation(&matTranslate, list[i].positionX, list[i].positionY, list[i].positionZ);				// passing all values to the translation matrix
		// create the final transform
		cBuffer.Final = matRotate * matTranslate * matView * matProjection;
		cBuffer.Rotation = matRotate;

		devcon->UpdateSubresource(pCBuffer, 0, 0, &cBuffer, 0, 0);

		// select which vertex buffer to display
		UINT stride = sizeof(Vertex);
		UINT offset = 0;
		devcon->IASetVertexBuffers(0, 1, &(list[i].buffer), &stride, &offset);
		devcon->IASetIndexBuffer(pIBuffer, DXGI_FORMAT_R32_UINT, 0);
		devcon->PSSetShaderResources(0, 1, &list[i].texture.tex);

		// select which primtive type we are using
		devcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// draw the vertex buffer to the back buffer
		devcon->DrawIndexed(54, 0, 0);
	}

	// switch the back buffer and the front buffer
	swapchain->Present(0, 0);
}

// Loads in the shader data
void InitPipeline()
{
	// load and compile the two shaders
	ID3D10Blob* errorBlob;
	ID3D10Blob *VS, *PS;
	D3DX11CompileFromFile("shaders.shader", 0, 0, "VShader", "vs_4_0", 0, 0, 0, &VS, 0, 0); // &errorBlob, 0);
	//char* errors = (char*)errorBlob->GetBufferPointer();
	D3DX11CompileFromFile("shaders.shader", 0, 0, "PShader", "ps_4_0", 0, 0, 0, &PS, 0, 0);

	// encapsulate both shaders into shader objects
	dev->CreateVertexShader(VS->GetBufferPointer(), VS->GetBufferSize(), NULL, &pVS);
	dev->CreatePixelShader(PS->GetBufferPointer(), PS->GetBufferSize(), NULL, &pPS);

	// set the shader objects
	devcon->VSSetShader(pVS, 0, 0);
	devcon->PSSetShader(pPS, 0, 0);

	// create the input layout object
	D3D11_INPUT_ELEMENT_DESC ied[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	dev->CreateInputLayout(ied, 3, VS->GetBufferPointer(), VS->GetBufferSize(), &pLayout);
	devcon->IASetInputLayout(pLayout);

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = 176; //64; //32; //16;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	dev->CreateBuffer(&bd, NULL, &pCBuffer);
	devcon->VSSetConstantBuffers(0, 1, &pCBuffer);
}