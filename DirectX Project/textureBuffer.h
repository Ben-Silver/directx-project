#pragma once

#include <D3D11.h>
#include <D3DX10.h>
#include <D3DX11.h>
#include <vector>

// This class will pass the name of a texture in as a char
// pointer so it can be applied to a single object
class textureBuffer
{
public:
	textureBuffer();
	~textureBuffer();

	// Name of the texture
	void texName(char *name);

	// setting the devices for the texture
	void setDevice(ID3D11Device *obj, ID3D11ShaderResourceView *tex);

	// Pointers
	ID3D11Device *obj;
	ID3D11ShaderResourceView *tex;

	// Variables
	int startX;		// Horizonal start of viewport
	int startY;		// Vertical start of viewport
	int width;		// Horizontal width of viewport
	int height;		// Horizontal height of viewport
};

