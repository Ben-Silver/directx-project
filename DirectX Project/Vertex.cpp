#include "Vertex.h"

// This class contains the template from which a vertex can be made

// Constructor
Vertex::Vertex(float angle)
{
	this->angle = angle;
}

// Overloaded Constructor
Vertex::Vertex(float x, float y, float z, D3DXVECTOR3 colour, float u, float v) // D3DXCOLOR colour)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->colour = colour;
	this->u = u;
	this->v = v;

}

// Destructor
Vertex::~Vertex()
{

}
