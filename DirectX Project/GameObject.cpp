#include "GameObject.h"

// Constructor
gameObject::gameObject(ID3D11Device* dev, ID3D11DeviceContext *devcon, std::vector<Vertex> ourVertices)
{
	this->dev = dev;
	this->devcon = devcon;
	this->ourVertices = ourVertices;

	jumpCount = 0;

	makeBuffer();
	canJump = true;
}

// Getters and Setters
void gameObject::setPlayerX(float pX)
{
	positionX = pX;
}
float gameObject::getPlayerX(void)
{
	return positionX;
}

void gameObject::setPlayerY(float pY)
{
	positionY = pY;
}
float gameObject::getPlayerY(void)
{
	return positionY;
}

void gameObject::setPlayerZ(float pZ)
{
	positionZ = pZ;
}
float gameObject::getPlayerZ(void)
{
	return positionZ;
}

void gameObject::setPlayerAngle(float pA)
{
	angle = pA;
}
float gameObject::getPlayerAngle(void)
{
	return angle;
}

void gameObject::setSpeed(float S)
{
	objSpeed = S;
}
float gameObject::getSpeed(void)
{
	return objSpeed;
}

void gameObject::setVertSpeed(float vS)
{
	vertSpeed = vS;
}
float gameObject::getVertSpeed(void)
{
	return vertSpeed;
}

void gameObject::setSpeedUp(bool sU)
{
	speedUp = sU;
}
bool gameObject::getSpeedUp(void)
{
	return speedUp;
}

void gameObject::setSlowDown(bool sD)
{
	slowDown = sD;
}
bool gameObject::getSlowDown(void)
{
	return slowDown;
}

void gameObject::setTurn(bool t)
{
	turn = t;
}
bool gameObject::getTurn(void)
{
	return turn;
}

void gameObject::setTurnLeft(bool tL)
{
	turnLeft = tL;
}
bool gameObject::getTurnLeft(void)
{
	return turnLeft;
}

void gameObject::setTurnRight(bool tR)
{
	turnRight = tR;
}
bool gameObject::getTurnRight(void)
{
	return turnRight;
}

void gameObject::setDPressed(bool dP)
{
	wPressed = dP;
}
bool gameObject::getDPressed(void)
{
	return wPressed;
}

void gameObject::setAPressed(bool aP)
{
	sPressed = aP;
}
bool gameObject::getAPressed(void)
{
	return sPressed;
}

void gameObject::setJump(bool J)
{
	jumped = J;
}
bool gameObject::getJump(void)
{
	return jumped;
}

void gameObject::setJumping(bool jP)
{
	playerJumping = jP;
}
bool gameObject::getJumping(void)
{
	return playerJumping;
}

// Destructor
gameObject::~gameObject()
{

}

// Vertex buffer
void gameObject::makeBuffer()
{
	//Initialize Vertex Buffer

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;														// write access access by CPU and GPU
	bd.ByteWidth = sizeof(Vertex) * ourVertices.size();									// size is the VERTEX struct * 3
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;											// use as a vertex buffer
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;											// allow CPU to write in buffer

	dev->CreateBuffer(&bd, NULL, &buffer);												// create the buffer

	//Upload the mesh data into the buffer
	D3D11_MAPPED_SUBRESOURCE ms;
	devcon->Map(buffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);						// map the buffer
	memcpy(ms.pData, &(ourVertices[0]), ourVertices.size() * sizeof(Vertex));           // copy the data
	devcon->Unmap(buffer, NULL);

}