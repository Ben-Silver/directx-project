#pragma once
//#include <D3DX10.h>

#include "textureBuffer.h"

// This class contains the template from which a vertex can be made

class Vertex
{
	public:
		// Constructor
		Vertex(float angle);

		// Overloaded Constructor
		Vertex(float x, float y, float z, D3DXVECTOR3 colour, float u, float v);
		
		// Destructor
		~Vertex();

		// Variables
		float x;
		float y;
		float z;
		D3DXVECTOR3 colour;
		float u;
		float v;
		float angle;
		textureBuffer texture;
};

