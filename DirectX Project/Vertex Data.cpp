#include "GameObject.h"
#include "Vertex.h"
#include "Vertex Data.h"

vertexData::vertexData()
{
	parallaxVerts.push_back(Vertex(-100.0f, -100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));
	parallaxVerts.push_back(Vertex(-100.0f, 100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	parallaxVerts.push_back(Vertex(100.0f, -100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	parallaxVerts.push_back(Vertex(100.0f, 100.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	scoreVerts.push_back(Vertex(-0.6f, -0.6f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));
	scoreVerts.push_back(Vertex(-0.6f, 0.6f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	scoreVerts.push_back(Vertex(2.0f, -0.6f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	scoreVerts.push_back(Vertex(2.0f, 0.6f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	scoreNumVerts.push_back(Vertex(-0.2f, -0.2f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));
	scoreNumVerts.push_back(Vertex(-0.2f, 0.2f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	scoreNumVerts.push_back(Vertex(0.2f, -0.2f, -2.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	scoreNumVerts.push_back(Vertex(0.2f, 0.2f, -1.8f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 0.0f));    // side 1
	cubeVerts.push_back(Vertex(1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));    // side 2
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 0.0f));   // side 3
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, -1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, 1.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f));    // side 4
	cubeVerts.push_back(Vertex(1.0f, -1.0f, -1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(-1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, -1.0f, 1.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(1.0f, -1.0f, -1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 5
	cubeVerts.push_back(Vertex(1.0f, 1.0f, -1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(1.0f, -1.0f, 1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(1.0f, 1.0f, 1.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	cubeVerts.push_back(Vertex(-1.0f, -1.0f, -1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 6
	cubeVerts.push_back(Vertex(-1.0f, -1.0f, 1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, -1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	cubeVerts.push_back(Vertex(-1.0f, 1.0f, 1.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f));    // base
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));    // side 1
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, -0.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, -1.5f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, -0.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(1.5f, -1.0f, -1.5f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 2
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, -0.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, 1.5f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, 0.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 0.0f));    // side 3
	pyramidVerts.push_back(Vertex(1.5f, -1.0f, 1.5f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, 0.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(0.0f, 1.5f, 0.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 1.0f));

	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, -1.5f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 4
	pyramidVerts.push_back(Vertex(-1.5f, -1.0f, 1.5f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, -0.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	pyramidVerts.push_back(Vertex(-0.0f, 1.5f, 0.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 0.0f));    // side 1
	platVerts.push_back(Vertex(5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(-5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 0.0f, 1.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 0.0f));    // side 2
	platVerts.push_back(Vertex(-5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 0.0f, -1.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 0.0f));   // side 3 - Top Base
	platVerts.push_back(Vertex(-5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, -5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, 5.0f, D3DXVECTOR3(0.0f, 1.0f, 0.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 0.0f));    // side 4
	platVerts.push_back(Vertex(5.0f, -1.0f, -5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(-5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, -1.0f, 5.0f, D3DXVECTOR3(0.0f, -1.0f, 0.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(5.0f, -1.0f, -5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 5
	platVerts.push_back(Vertex(5.0f, 1.0f, -5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(5.0f, -1.0f, 5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(5.0f, 1.0f, 5.0f, D3DXVECTOR3(1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	platVerts.push_back(Vertex(-5.0f, -1.0f, -5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 6
	platVerts.push_back(Vertex(-5.0f, -1.0f, 5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 0.0f, 1.0f));
	platVerts.push_back(Vertex(-5.0f, 1.0f, -5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 0.0f));
	platVerts.push_back(Vertex(-5.0f, 1.0f, 5.0f, D3DXVECTOR3(-1.0f, 0.0f, 0.0f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 0.0f, 0.0f));    // side 1
	bulletVerts.push_back(Vertex(0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.0f, 0.3f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 0.0f, 0.0f));    // side 2
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.0f, -0.3f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 0.0f, 0.0f));   // side 3
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.0f, 0.3f, 0.0f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 0.0f, 0.0f));    // side 4
	bulletVerts.push_back(Vertex(0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(-0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.0f, -0.3f, 0.0f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(0.3f, -0.3f, -0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 5
	bulletVerts.push_back(Vertex(0.3f, 0.3f, -0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(0.3f, -0.3f, 0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(0.3f, 0.3f, 0.3f, D3DXVECTOR3(0.3f, 0.0f, 0.0f), 1.0f, 1.0f));

	bulletVerts.push_back(Vertex(-0.3f, -0.3f, -0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 0.0f, 0.0f));    // side 6
	bulletVerts.push_back(Vertex(-0.3f, -0.3f, 0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 0.0f, 1.0f));
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, -0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 1.0f, 0.0f));
	bulletVerts.push_back(Vertex(-0.3f, 0.3f, 0.3f, D3DXVECTOR3(-0.3f, 0.0f, 0.0f), 1.0f, 1.0f));

	// Initialise Game Objects using vertices
	gameObject player(dev, devcon, pyramidVerts); // cubeVerts);
	gameObject platform(dev, devcon, platVerts);
	gameObject platform2(dev, devcon, platVerts);
	gameObject platform3(dev, devcon, platVerts);
	gameObject platform4(dev, devcon, platVerts);
	gameObject platform5(dev, devcon, platVerts);
	gameObject platform6(dev, devcon, platVerts);
	gameObject platform7(dev, devcon, platVerts);
	gameObject goal(dev, devcon, cubeVerts);
	gameObject parallax(dev, devcon, parallaxVerts);
	gameObject scoreBox(dev, devcon, scoreVerts);
	gameObject turret1(dev, devcon, cubeVerts);
	gameObject bullet1(dev, devcon, bulletVerts);
	gameObject bullet2(dev, devcon, bulletVerts);
	gameObject bullet3(dev, devcon, bulletVerts);
	gameObject scoreNum0(dev, devcon, scoreNumVerts);
	gameObject scoreNum1(dev, devcon, scoreNumVerts);
	gameObject scoreNum2(dev, devcon, scoreNumVerts);
	gameObject scoreNum3(dev, devcon, scoreNumVerts);
	gameObject scoreNum4(dev, devcon, scoreNumVerts);
	gameObject scoreNum5(dev, devcon, scoreNumVerts);
	gameObject scoreNum6(dev, devcon, scoreNumVerts);
	gameObject scoreNum7(dev, devcon, scoreNumVerts);
	gameObject scoreNum8(dev, devcon, scoreNumVerts);
	gameObject scoreNum9(dev, devcon, scoreNumVerts);

	positionX = 0.0f;
	positionY = 0.0f;
	positionZ = 0.0f;
}