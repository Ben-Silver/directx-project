#pragma once
#include <D3D11.h>
#include <D3DX10.h>
#include <D3DX11.h>
#include <vector>

#include "Vertex.h"
#include "textureBuffer.h"

// This class contains the vertexBuffer and all the getters, setters and variables for all the objects in the game
class gameObject
{
	public:
		// Contructor prototype
		gameObject(ID3D11Device* dev, ID3D11DeviceContext *devcon, std::vector<Vertex> ourVertices);

		// Getter and Setter prototypes
		void setPlayerX(float pX);
		float getPlayerX(void);
		void setPlayerY(float pY);
		float getPlayerY(void);
		void setPlayerZ(float pZ);
		float getPlayerZ(void);
		void setPlayerAngle(float pA);
		float getPlayerAngle(void);
		void setSpeed(float S);
		float getSpeed(void);
		void setVertSpeed(float vS);
		float getVertSpeed(void);
		void setSpeedUp(bool sU);
		bool getSpeedUp(void);
		void setSlowDown(bool sD);
		bool getSlowDown(void);
		void setTurn(bool t);
		bool getTurn(void);
		void setTurnLeft(bool tL);
		bool getTurnLeft(void);
		void setTurnRight(bool tR);
		bool getTurnRight(void);
		void setDPressed(bool dP);
		bool getDPressed(void);
		void setAPressed(bool aP);
		bool getAPressed(void);
		void setJump(bool J);
		bool getJump(void);
		void setJumping(bool jP);
		bool getJumping(void);
		
		// Destructor
		~gameObject();

		// Vertex Buffer prototype
		void makeBuffer();
		
		// D3DX Pointers
		ID3D11Buffer* buffer;
		ID3D11Device* dev;
		ID3D11DeviceContext *devcon;
		std::vector<Vertex> ourVertices;
		textureBuffer texture;

		// Variables
		float positionX;
		float positionY;
		float positionZ;
		float angle;
		float objSpeed;
		float vertSpeed;
		float fall;
		bool speedUp;
		bool slowDown;
		bool turn;
		bool turnLeft;
		bool turnRight;
		bool wPressed;
		bool sPressed;
		bool jumped;
		bool playerJumping;
		float jumpCount;
		bool canJump;
		
};

